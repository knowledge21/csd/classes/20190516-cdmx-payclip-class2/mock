import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
@RunWith(MockitoJUnitRunner.class)
public class TestRoyaltyCalculator {
    @Mock
    private SalesRepository salesRepository;
    @InjectMocks
    private RoyaltyCalculator royaltyCalculator;

    @Test
    public void testNoSalesForTheMonth(){
        int month = 1;
        int year = 2019;

        double expected = 0;
        Mockito.when(salesRepository.getSales(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(Collections.emptyList());
        double result = royaltyCalculator.getRoyalty(month,year);

        Assert.assertEquals(expected, result, 0);

    }
    @Test
    public void testSaleOf100ForTheMonth(){
        int month = 2;
        int year = 2019;

        double expected = 19;

        double restult = new RoyaltyCalculator(salesRepository).getRoyalty(month,year);

        Assert.assertEquals(expected, restult, 0);

    }
    @Test
    public void testWith2SalesForTheMonth(){
        int month = 3;
        int year = 2019;

        double expected = 3780.18;

        Mockito.when(salesRepository.getSales(Mockito.any(Integer.class), Mockito.any(Integer.class))).thenReturn(Arrays.asList(10000.00, 10001.0));
        double restult = new RoyaltyCalculator(salesRepository).getRoyalty(month,year);

        Assert.assertEquals(expected, restult, 0);

    }
}
