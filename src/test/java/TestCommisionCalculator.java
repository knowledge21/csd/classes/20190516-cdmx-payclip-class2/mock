import org.junit.Assert;
import org.junit.Test;

public class TestCommisionCalculator {

    @Test
    public void test10kSaleReturn500Commission() {

        int sale = 10000;
        int commission = 500;

        double returnValue = CommissionCalculator.calculate(sale);

        Assert.assertEquals(commission, returnValue, 0);
    }

    @Test
    public void test10001SaleReturn600_06Commission() {

        int sale = 10001;
        double commission = 600.06;

        double returnValue = CommissionCalculator.calculate(sale);

        Assert.assertEquals(commission, returnValue, 0);
    }

    @Test
    public void test20kSaleReturn1200Commission() {

        int sale = 20000;
        double commission = 1200;

        double returnValue = CommissionCalculator.calculate(sale);

        Assert.assertEquals(commission, returnValue, 0);
    }

    @Test
    public void test5kSaleReturn250Commission() {

        int sale = 5000;
        int commission = 250;

        double returnValue = CommissionCalculator.calculate(sale);

        Assert.assertEquals(commission, returnValue, 0);
    }

    @Test
    public void test55_59SaleReturn2_77Commission() {

        double sale = 55.59;
        double commission = 2.77;

        double returnValue = CommissionCalculator.calculate(sale);

        Assert.assertEquals(commission, returnValue, 0);
    }

    @Test
    public void test21547_59SaleReturn1292_85Commission() {

        double sale = 21547.59;
        double commission = 1292.85;

        double returnValue = CommissionCalculator.calculate(sale);

        Assert.assertEquals(commission, returnValue, 0);
    }

}
