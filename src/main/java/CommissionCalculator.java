import java.math.RoundingMode;
import java.text.DecimalFormat;

public class CommissionCalculator {

    private static final int LOWER_SALE_VALUE_THRESHOLD = 10000;
    private static final double LOWER_COMMISSION_PERCENTAGE = 0.05;
    private static final double HIGHER_COMMISSION_PERCENTAGE = 0.06;

    public static double truncateToTwoDecimalPlaces(double sale) {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.FLOOR);
        return Double.valueOf(df.format(sale));
    }

    public static double calculate(double sale) {
        if (sale <= LOWER_SALE_VALUE_THRESHOLD) {
            return truncateToTwoDecimalPlaces(sale * LOWER_COMMISSION_PERCENTAGE);
        }
        return truncateToTwoDecimalPlaces(sale * HIGHER_COMMISSION_PERCENTAGE);
    }
}
