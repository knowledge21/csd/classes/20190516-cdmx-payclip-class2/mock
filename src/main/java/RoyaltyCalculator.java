import java.util.List;

public class RoyaltyCalculator {
    private SalesRepository salesRepository;

    public RoyaltyCalculator(SalesRepository salesRepository){
        this.salesRepository = salesRepository;
    }

    public double getRoyalty(Integer month, Integer year) {
        List<Double> results =  salesRepository.getSales(year, month);
        double result = 0;
        for(double item : results) {
            result += item-CommissionCalculator.calculate(item);
        }
        return CommissionCalculator.truncateToTwoDecimalPlaces(result*.20);
    }

}
